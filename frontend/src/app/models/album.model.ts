import {Artist} from "./artist.model";

export class Album {
  constructor(
    public _id: string,
    public name: string,
    public artist: Artist,
    public year: string,
    public image: string,
  ){}
}

export interface ApiAlbumData {
  _id: string,
  name: string,
  artist: Artist,
  year: string,
  image: string
}