import {Album} from "./album.model";

export class Track{
  constructor(
    public _id: string,
    public name: string,
    public album: Album,
    public duration: string
  ) {}
}

export interface ApiTrackData {
  _id: string,
  name: string,
  album: Album,
  duration: string,
}