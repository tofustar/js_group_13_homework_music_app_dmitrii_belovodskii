import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {TrackHistoryService} from "../services/trackHistory.service";
import {
  addToTrackHFailure,
  addToTrackHRequest,
  addToTrackHSuccess,
  fetchTrackHFailure,
  fetchTrackHRequest,
  fetchTrackHSuccess
} from "./trackHistory.actions";
import {catchError, map, mergeMap, NEVER, of, withLatestFrom} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "./types";
import {Router} from "@angular/router";

@Injectable()

export class TrackHistoryEffects {
  constructor(
    private actions: Actions,
    private trackHistoryService: TrackHistoryService,
    private store: Store<AppState>,
    private router: Router,
  ) {}

  addToTrackHistory = createEffect(() => this.actions.pipe(
      ofType(addToTrackHRequest),
      withLatestFrom(this.store.select(state =>  state.users.user)),
      mergeMap(([{idTrack}, user]) => {
        if (user) {
          return this.trackHistoryService.addToTrackHistory(user.token, idTrack).pipe(
            map(() => addToTrackHSuccess()),
            catchError(() => of(addToTrackHFailure({error: 'Something went wrong'})))
          );
        }
        return NEVER;
      })
    )
  )

  fetchTrackHistory = createEffect(() => this.actions.pipe(
    ofType(fetchTrackHRequest),
    withLatestFrom(this.store.select(state => state.users.user)),
    mergeMap(([_, user]) => {
      if(user) {
        return this.trackHistoryService.getTrackHistory(user.token).pipe(
          map(trackHistory =>  fetchTrackHSuccess({trackHistory})),
          catchError(() => of(fetchTrackHFailure({error: 'Something went wrong'})))
        );
      }
      void this.router.navigate(['/']);
      return NEVER;
    })
  ));

}