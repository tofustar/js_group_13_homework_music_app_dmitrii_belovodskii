const express = require('express');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', async (req, res, next) => {
  try{
    const filter = {};

    if(req.query.album) {
      filter.album = req.query.album;
    }

    const tracks = await Track.find(filter);
    return res.send(tracks);
  } catch(e) {
    next(e);
  }
});

router.post('/', auth, permit('admin'), async (req, res, next) => {
  try{

    if(!req.body.name && !req.body.album) {
      return res.status(404).send('Track name and album are required')
    }

   const trackData = {
     name: req.body.name,
     album: req.body.album,
     duration: req.body.duration,
   }

   const track = new Track(trackData);

   await track.save();

   return res.send(track);

  } catch(e) {
    next(e);
  }
});

module.exports = router;