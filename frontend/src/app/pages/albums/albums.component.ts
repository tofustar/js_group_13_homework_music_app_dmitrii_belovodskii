import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Album} from "../../models/album.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/types";
import {fetchAlbumByArtistRequest} from "../../store/albums.actions";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.sass']
})
export class AlbumsComponent implements OnInit {
  idArtist: Observable<string>;
  albums: Observable<Album[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  artistName = '';

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.idArtist = store.select(state => state.albums.albumByArtistId);
    this.albums = store.select(state => state.albums.albums);
    this.loading = store.select(state => state.albums.fetchLoading);
    this.error = store.select(state => state.albums.fetchError);
  }

  ngOnInit(): void {
    const idArtist = this.route.snapshot.params['id'];
    this.store.dispatch(fetchAlbumByArtistRequest({idArtist}));
    this.albums.subscribe(albumData => {
      albumData.forEach(album => {
        this.artistName = album.artist.name;
      })
    })
  }
}
