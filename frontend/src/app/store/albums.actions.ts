import {createAction, props} from "@ngrx/store";
import {Album} from "../models/album.model";

export const fetchAlbumsRequest = createAction('[Albums] Fetch Request');
export const fetchAlbumsSuccess = createAction('[Albums] Fetch Success', props<{albums: Album[]}>());
export const fetchAlbumsFailure = createAction('[Albums] Fetch Failure', props<{error: string}>());

export const fetchAlbumByArtistRequest = createAction('[AlbumsByArtist] Fetch Request', props<{idArtist: string}>());
export const fetchAlbumByArtistSuccess = createAction('[AlbumsByArtist] Fetch Success', props<{albums: Album[]}>());
export const fetchAlbumByArtistFailure = createAction('[AlbumsByArtist] Fetch Failure', props<{error: string}>());