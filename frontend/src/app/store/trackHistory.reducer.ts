import {TrackHistoryState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  addToTrackHFailure,
  addToTrackHRequest,
  addToTrackHSuccess, fetchTrackHFailure,
  fetchTrackHRequest,
  fetchTrackHSuccess
} from "./trackHistory.actions";

const initialState: TrackHistoryState = {
  trackHistory: [],
  idTrack: '',
  fetchLoading: false,
  fetchError: null,
  addToTHLoading: false,
  addToTHError: null,
};

export const trackHistoriesReducer = createReducer(
  initialState,
  on(addToTrackHRequest, (state, {idTrack}) => ({...state, addToTHLoading: true, idTrack})),
  on(addToTrackHSuccess, (state) => ({...state, addToTHLoading: false})),
  on(addToTrackHFailure, (state, {error}) => ({...state, addToTHLoading: false, addToTHError: error})),

  on(fetchTrackHRequest, state => ({...state, fetchLoading: true})),
  on(fetchTrackHSuccess, (state, {trackHistory}) => ({...state, fetchLoading: false, trackHistory})),
  on(fetchTrackHFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),
)