const express = require('express');
const auth = require('../middleware/auth');
const TrackHistory = require('../models/TrackHistory');

const router = express.Router();

router.get('/', auth, async (req, res, next) => {
  try{

    const trackHistory = await TrackHistory.find({user: req.user._id}).populate({
      path: 'track',
      select: 'name',
      populate: {
        path: 'album',
        populate: {
          path: 'artist',
          select: 'name',
        }
      }
    });

    return res.send(trackHistory);

  } catch (e) {
    return next(e);
  }
})

router.post('/', auth, async (req, res, next) => {
  try {

    const dateTime = new Date().toLocaleString();

    const trackHistoryData = {
      user: req.user,
      track: req.body.idTrack,
      datetime: dateTime,
    };

    const trackHistory = new TrackHistory(trackHistoryData);

    await trackHistory.save();

    return res.send(trackHistory);

  } catch (e) {
    return next(e);
  }
});

module.exports = router;