import {AlbumsState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  fetchAlbumByArtistFailure,
  fetchAlbumByArtistRequest,
  fetchAlbumByArtistSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess
} from "./albums.actions";

const initialState: AlbumsState = {
  albums: [],
  albumByArtistId: '',
  fetchLoading: false,
  fetchError: null,
};

export const albumsReducer = createReducer(
  initialState,
  on(fetchAlbumsRequest, state => ({...state, fetchLoading: true})),
  on(fetchAlbumsSuccess, (state, {albums}) => ({...state, fetchLoading: false, albums})),
  on(fetchAlbumsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchAlbumByArtistRequest, (state, {idArtist}) => ({...state, fetchLoading: true, albumByArtistId: idArtist})),
  on(fetchAlbumByArtistSuccess, (state, {albums}) => ({...state, fetchLoading: false, albums})),
  on(fetchAlbumByArtistFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),
)