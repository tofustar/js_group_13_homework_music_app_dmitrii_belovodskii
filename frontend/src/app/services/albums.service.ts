import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ApiAlbumData} from "../models/album.model";
import {environment as env} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class AlbumsService {

  constructor(private http: HttpClient) {}

  getAlbums() {
    return this.http.get<ApiAlbumData[]>(env.apiUrl + '/albums');

  }

  getAlbumsByArtist(artistId: string) {
    return this.http.get<ApiAlbumData[]>(env.apiUrl + '/albums?artist=' + artistId);
  }
}