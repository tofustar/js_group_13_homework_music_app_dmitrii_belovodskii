const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try{
    const filter = {};

    if(req.query.artist) {
      filter.artist = req.query.artist;
    }

    const albums = await Album.find(filter).populate('artist', 'name');

    return res.send(albums);
  } catch(e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const album = await Album.findById(req.params.id).populate('artist', 'name image info');

    if(!album) {
      return res.status(404).send({message: 'Album not found'});
    }

    return res.send(album);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, permit('admin'), upload.single('image'), async (req, res, next) => {
  try {

    if(!req.body.name && !req.body.artist) {
      return res.status(400).send('Name and artist are required');
    }

    const albumData = {
      name: req.body.name,
      artist: req.body.artist,
      year: req.body.year,
      image: null,
    };

    if(req.file) {
      albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    await album.save();

    return res.send(album);

  } catch(e) {
    next(e);
  }
})

module.exports = router;