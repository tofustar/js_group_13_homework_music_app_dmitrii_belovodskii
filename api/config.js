const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  mongo: {
    db: 'mongodb://localhost/music-app',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '735277287831997',
    appSecret: '9301d651565bc78819dca18f3715f45b'
  }
};