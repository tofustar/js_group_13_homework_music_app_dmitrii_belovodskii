import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {TracksService} from "../services/tracks.service";
import {
  fetchTracksByAlbumFailure,
  fetchTracksByAlbumRequest,
  fetchTracksByAlbumSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess
} from "./tracks.actions";
import {catchError, map, mergeMap, of} from "rxjs";

@Injectable()

export class TracksEffects {

  constructor(
    private actions: Actions,
    private tracksService: TracksService,
  ) {}

  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(() => this.tracksService.getTracks().pipe(
      map(tracks => fetchTracksSuccess({tracks})),
      catchError(() => of(fetchTracksFailure({error: 'Something went wrong'})))
    ))
  ));

   fetchTracksByAlbumId = createEffect(() => this.actions.pipe(
    ofType(fetchTracksByAlbumRequest),
    mergeMap(({idAlbum}) => this.tracksService.getTracksByAlbum(idAlbum).pipe(
      map(tracks => fetchTracksByAlbumSuccess({tracks})),
      catchError(() => of(fetchTracksByAlbumFailure({error: 'Something went wrong'})))
    ))
  ));
}