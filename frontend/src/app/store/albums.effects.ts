import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {AlbumsService} from "../services/albums.service";
import {
  fetchAlbumByArtistFailure,
  fetchAlbumByArtistRequest,
  fetchAlbumByArtistSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess
} from "./albums.actions";
import {catchError, map, mergeMap, of} from "rxjs";

@Injectable()

export class AlbumsEffects {

  constructor(
    private actions: Actions,
    private albumsService: AlbumsService,
  ) {}

  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(() => this.albumsService.getAlbums().pipe(
      map(albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchAlbumByArtistId = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumByArtistRequest),
    mergeMap( ({idArtist}) => this.albumsService.getAlbumsByArtist(idArtist).pipe(
      map(albums => fetchAlbumByArtistSuccess({albums})),
      catchError(() => of(fetchAlbumByArtistFailure({error: 'Something went wrong'})))
    ))
  ));
}